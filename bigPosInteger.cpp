/*
 * Lab 2 Big Positive Integer
 * Name:
 * Lab Section(Tuesday / Thursday):
 */

#include "bigPosInteger.h"

int lengthOfNumber(int number);

bigPosInteger::bigPosInteger() {
    valueArray = NULL;
    length = 0;
}

bigPosInteger::bigPosInteger(std::string value)
/*This constructor should take in a string containing a set of chars between '0' and '9' of arbitrary length and constructs it into bigPosInteger type*/
{
    length = value.length();
    valueArray = new int[length];

    for(int i = value.length()-1; i >= 0; i--){
        valueArray[(length-1)-i] = value[i] - '0';//puts most significant digit into highest index
    }
}

bigPosInteger::bigPosInteger(int value)
/*this instructor takes in an integer and constructs it into a bigPosInteger type*/
{
    if(value == 0){
        length = 1;
        valueArray = new int[1];
        valueArray[0] = 0;
    }
    else {
        length = lengthOfNumber(value);
        valueArray = new int[length];
        int position = 0;
        while (value > 0) {//separate the integers digits into the array
            valueArray[position] = value % 10;
            value = value / 10;
            position++;
        }
    }
}

bigPosInteger::bigPosInteger(const bigPosInteger& value)
/*This is a copy constructor, be EXTREMELY careful for memory leaks here*/
{
    length = value.length;
    valueArray = new int[length];
    for(int i = 0; i < length; i++){
        valueArray[i] = value.valueArray[i];
    }

}

bigPosInteger::~bigPosInteger()
/*This is the destructor, be extremely careful for memory leaks here*/
{
    if(valueArray!=NULL)
        delete [] valueArray;
}

bigPosInteger bigPosInteger::operator+ (const bigPosInteger& rhs)
/*this operator should be able to add two bigPosInteger together and return the result. The default return should be replaced with the appropriate variable*/
{
    bigPosInteger sum;

    if(this->length > rhs.length){
        sum = rhs.sumFormatted(*this);
    }
    else{
        sum = (*this).sumFormatted(rhs);
    }
    return sum;
}

bigPosInteger bigPosInteger::operator- (const bigPosInteger& rhs)
/*this operator should be able to subtract the Right Hand Side bigPosInteger from the base bigPosInteger and return the result. The default return should be replaced with the appropriate variable*/
{
    if(*this < rhs)
    {
        std::cout << "Error: Result is negative" << std::endl;
        throw 1;
    }
    bigPosInteger difference=*this;
    int digitDifference,borrow=0;

    for(int position=0; position < difference.length; ++position){
        if(position < rhs.length)
            digitDifference = (difference.valueArray[position]-rhs.valueArray[position])-borrow;
        else
            digitDifference = difference.valueArray[position]-borrow;
        if(digitDifference < 0){
            difference.valueArray[position]=digitDifference + 10;
            borrow=1;
        } else{
            difference.valueArray[position]=digitDifference;
            borrow=0;
        }
    }

    while (difference.valueArray[difference.length-1] == 0 && difference.length > 1)
        difference.extendAndCopyArray(-1);//get rid of significant digit zero

    return difference;
}

bigPosInteger bigPosInteger::operator*(const bigPosInteger& rhs)
/*this operator should be able to multiply two bigPosInteger together and return the result. The default return should be replaced with the appropriate variable*/
{

    std::string zeroes = "";
    std::string s = "";
    int temp_prod = 0;
    bigPosInteger product("000000");
    bigPosInteger temp("00000");

    product.length = rhs.length + length;
    delete [] product.valueArray;
    product.valueArray = new int[product.length];
    for(int i = 0; i < product.length; i++){
        product.valueArray[i] = 0;
    }

    for(int i = 0; i < rhs.length; i++){

        for(int k = 0; k < i; k++){
            zeroes = zeroes + "0";
        }
        for(int j = 0; j < length; j++){


            s = "";
            temp_prod = rhs.valueArray[i] * valueArray[j];
            s = toString(temp_prod) + zeroes;

            temp = bigPosInteger(s);
            product = (product + temp);
            zeroes += "0";
        }
        zeroes = "";
    }

    return product;
}

std::string bigPosInteger::toString(int a){
    std::string s = " ";
    s = std::to_string(a);
    return s;
}

bigPosInteger &bigPosInteger::operator=(const bigPosInteger& rhs)
/* this is the copy assignment operator, be EXTREMELY careful for memory leaks here. The default return should be replaced with the appropriate variable*/
{
    if(this == &rhs){
        return *this;
    }
    else{
        if (valueArray !=NULL)
            delete [] valueArray;
        length = rhs.length;
        valueArray = new int[length];
        for(int i = 0; i < length; i++){
            valueArray[i] = rhs.valueArray[i];
        }
    }
    return *this;

}

std::ostream &operator<<(std::ostream & stream, const bigPosInteger& rhs)
/* this is the copy assignment operator, be EXTREMELY careful for memory leaks here. The default return should be replaced with the appropriate variable*/
{
    for(int i = (rhs.length - 1); i >= 0; i--){
        stream << rhs.valueArray[i];
    }
    return stream;
}

//Added names to istream and to bigPosInteger
std::istream &operator>>(std::istream & istr, bigPosInteger & rhs)
/* this is the copy assignment operator, be EXTREMELY careful for memory leaks here. The default return should be replaced with the appropriate variable*/
{
    std::string s = "";
    int len = 0;
    istr >> s;

    delete [] rhs.valueArray;
    len = s.length();
    rhs.valueArray = new int[len];
    rhs.length = len;
    int x = 0;
    int num = 0;
    for(int i = (s.length()-1); i >= 0; i--){
        num = s[i] - '0';
        rhs.valueArray[x] = num;
        x++;
    }

    return istr;
}


bigPosInteger bigPosInteger::operator%(const bigPosInteger& rhs) {
    bigPosInteger remainder=*this;

    while (rhs < remainder) {
        remainder = remainder - rhs;
    }
    return remainder;
}


bool bigPosInteger::operator<(const bigPosInteger& rhs) const{
    if(length < rhs.length){
        return true;
    }
    else if(length > rhs.length){
        return false;
    }
    else{
        for(int i = (length - 1); i >= 0; i-- ){
            if(valueArray[i] < rhs.valueArray[i]){
                return true;
            }
            else if(valueArray[i] > rhs.valueArray[i]){
                return false;
            }
        }
    }
    return false; //integers are equal
}

bigPosInteger bigPosInteger::sumFormatted(bigPosInteger largerInteger) const {
    const bigPosInteger* smallerInteger =this;
    int carry =0, digitSum=0, position;

    for(position = 0; position < largerInteger.length; position++) {
        if(position < smallerInteger->length) {
            digitSum = largerInteger.valueArray[position] + smallerInteger->valueArray[position] + carry;
            largerInteger.valueArray[position] = digitSum % 10;
            carry = digitSum / 10;
        }
        else if(carry != 0){
            digitSum = largerInteger.valueArray[position] + carry;
            largerInteger.valueArray[position] = digitSum % 10;
            carry = digitSum / 10;
        }
    }
    if(carry != 0){
        largerInteger.extendAndCopyArray(1);
        largerInteger.valueArray[largerInteger.length-1] = carry;
    }
    return largerInteger;
}

void bigPosInteger::extendAndCopyArray(int addedLength) {
    if(length+addedLength<=0)return;
    int* tempArray;
    length+=addedLength;
    tempArray = new int[length];
    for(int i = 0; i < (length-addedLength); i++){
        tempArray[i] = valueArray[i];
    }
    delete [] valueArray;
    valueArray = tempArray;
}

int lengthOfNumber(int number){
    int count = 0;
    while (number > 0) {
        number = number / 10;
        count++;
    }
    return count;
}